![stability-workinprogress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)

# RATIONALE #

* This repo is -mainly for internal use-, but it can be useful for those who need to migrate data in the most *resilient*, *efficient* way. 
* This repository just hold our datums gathered, checklist _et alia_ and begin and end if fulfill our needs. Anyway, *one caveat*: "There is no responsability for data loss".
* This repo is a living document that will grow and adapt over time.
![18ixteo19glnmpng.png](https://bitbucket.org/repo/LoMoRKb/images/2581579234-18ixteo19glnmpng.png)

### What is this repository for? ###

* Quick summary
    - Migration of custom profiles and settings between differents MacOSX Operating systems, specifically from `El Capitan` to ~~Mojave~~ `High Sierra`
* Version 1.02

### How do I get set up? ###

* Summary of set up
    - _Vide_ [`colophon.md`](https://bitbucket.org/imhicihu/migration-data-between-different-macos-environments-checklist/src/master/Colophon.md) on `source` tab (at the left of your current screen sight)
* Configuration
    - _Vide_ [`checklist.md`](https://bitbucket.org/imhicihu/migration-data-between-different-macos-environments-checklist/src/master/Checklist.md)
* Deployment instructions
    - _Vide_ [`checklist.md`](https://bitbucket.org/imhicihu/migration-data-between-different-macos-environments-checklist/src/master/Checklist.md)

### Related repositories ###

* Some repositories linked with this project:
     - [Migration data (checklist)](https://bitbucket.org/imhicihu/migration-data-checklist/src/)
     - [Focus Group (2016)](https://bitbucket.org/imhicihu/focus-group-2016/src/default/)

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/migration-data-between-different-macos-environments-checklist/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/migration-data-between-different-macos-environments-checklist/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/migration-data-between-different-macos-environments-checklist/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/migration-data-between-different-macos-environments-checklist/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/migration-data-between-different-macos-environments-checklist/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)